<?php
	$thumb_id = get_post_thumbnail_id();
	$thumb_url = wp_get_attachment_url( $thumb_id );
	$c++;
?>
<div class="col-lg-12 col-md-12 col-sm-12 no-padding <?php if($c % 2 == '0'){echo 'par';}else{echo 'impar';} ?>">
	<div class="row">
		<div class="col-lg-4 col-md-4 col-sm-12 image">
			<figure>
				<img src="<?php echo $thumb_url; ?>">
			</figure>	
		</div>
		<div class="col-lg-8 col-md-8 col-sm-12 text">
			<div class="info">
				<h3><?php the_title(); ?></h3>
				<p><?php the_content(); ?></p>
			</div>
		</div>
	</div>
</div>