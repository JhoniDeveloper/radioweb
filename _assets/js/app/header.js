jQuery( document ).ready(function( $ ){

	jQuery(".menu_desk .menu-home-container ul").addClass('nav justify-content-end');
	jQuery(".mobile_menu .menu-home-container ul").addClass('nav justify-content-center flex-column');
	jQuery(".menu-home-container ul li").addClass('nav-item');
	jQuery(".menu-home-container ul li:first-child a").addClass('nav-link active');
	jQuery(".menu-home-container ul li a").addClass('nav-link');

	/*Search Box*/
	jQuery(".search-box").fadeOut();
	jQuery(".open_search").click(function( event ){
		jQuery(".search-box").fadeIn();
	});	
	jQuery(".search_close").click(function( event ){
		jQuery(".search-box").fadeOut();
	});

	jQuery('.mobile_menu').slideUp();

	jQuery('.hamburger').click(function(event){
		jQuery(this).toggleClass('is-active');
		jQuery('.mobile_menu').slideToggle();
	});
});