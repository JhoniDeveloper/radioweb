<style type="text/css">
	.single .single_content img{
		width: 100%;
		height: auto;
	}
</style>
<?php
	get_header();
	bg_page();
?>
<section class="single">
	<div class="container">
		<div class="row">
			<?php
				if (have_posts()):
					while (have_posts()):the_post();
						$thumb_id = get_post_thumbnail_id();
						$thumb_url = wp_get_attachment_url( $thumb_id );				
			?>
						<div class="col-lg-12 col-md-12 col-sm-12">
							<figure>
								<img src="<?php echo get_field('capa', get_the_ID()); ?>">
							</figure>
						</div>				
						<div class="col-lg-8 col-md-8 col-sm-12">
							
							<h1>
								<?php the_title(); ?>
							</h1>							
							<div class="single_content"><p><?php the_content(); ?></p></div>
						</div>
						<div class="col-lg-4 col-md-4 col-sm-12 randomicos posts">
							<div class="post_autor">
								<div class="row">
										<div class="col-lg-2 col-md-3 col-sm-2">
											<?php echo get_avatar( get_the_author_ID(), 40 ) ?>
										</div>	
										<div class="col-lg-10 col-md-9 col-sm-10">
											<?php the_author(); ?><br>
											<?php echo get_the_date(); ?>						
										</div>										
								</div>														
							</div>
							
							<h2 style="margin-top: 4%;">Outros Artigos</h2>
							<?php
								// $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
								$args = array(
									'post_type' 	=> 'post',
									'order'     	=> 'DESC',
									'orderby'		=> 'rand',
									'cat'			=> '-33',
									'posts_per_page'=> '3',
									'post_status'	=> 'publish'
								);
								$wc_query = new WP_Query( $args );

					            if ($wc_query -> have_posts()):
					                while ($wc_query -> have_posts()): $wc_query -> the_post();
										$thumb_id = get_post_thumbnail_id();
										$thumb_url = wp_get_attachment_url( $thumb_id );	
										$cat = get_the_category( get_the_ID() );	                	
							?>
								<a href="<?php the_permalink(); ?>">
									<div class="artigo">					
										<article>
											<figure>
												<img src="<?php echo $thumb_url; ?>">
											</figure>
											<div class="desc">
												<div class="align_Desc"><p><?php echo esc_html($cat[0]->name); ?></p></div>
												<h3><?php the_title(); ?></h3>
											</div>									
										</article>
									</div>
								</a>
							<?php
								endwhile;				
								endif;
								wp_reset_query();
							?>					
						</div>						
			<?php
					endwhile;
				endif;
			?>
		</div>
	</div>
</section>
<section class="comments_area">
	<div class="container">
		<h1 class="title">COMENTÁRIOS</h1>
		<div class="row avaliacoes">
			<?php comments_template(); ?>
		</div>
	</div>
</section> 
<section class="recentes">
	<div class="container">
		<h1 class="title">ARTIGOS RECENTES</h1>
		<div class="row">
			<?php
				$args = array(
					'post_status'	=> 'publish',
					'posts_per_page'=> '4',
					'cat'			=> '-33',
					'order'			=> 'ASC',
					'orderby'		=> 'date'
				);				
				$wc_query = new WP_Query( $args );
				if ($wc_query -> have_posts()):
					while ($wc_query -> have_posts()): $wc_query -> the_post();
						include('inc/loop-blog.php');
					endwhile;
				endif;
			?>			
		</div>
	</div>
</section>
<?php
	get_footer();
?>