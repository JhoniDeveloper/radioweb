<?php
	get_header();
	bg_page();
?>

<section class="recentes" style="margin-bottom: 4%;">
	<div class="container">
		<h2 style="margin-left: 1.5%;">RESULTADOS DA BUSCA POR: <?php echo get_search_query(); ?></h2>		
		
		<div class="row posts">
			<?php			
				if (have_posts()):
					while (have_posts()): the_post();
						$thumb_id = get_post_thumbnail_id();
						$thumb_url = wp_get_attachment_url( $thumb_id );	
						$cat = get_the_category( get_the_ID() );						
?>
			<div class="col-lg-4 col-md-4 col-sm-12 col-12 no-padding-right">
				<a href="<?php the_permalink(); ?>">
					<div class="artigo">					
						<article>
							<figure>
								<img src="<?php echo $thumb_url; ?>">
							</figure>
							<div class="desc">
								<div class="align_Desc"><p><?php echo esc_html($cat[0]->name); ?></p></div>
								<h3><?php the_title(); ?></h3>								
							</div>				
						</article>
					</div>
				</a>
			</div>
<?php
					endwhile;
					the_posts_pagination();
				endif;
			?>			
		</div>
	</div>
</section>
<?php
	get_footer();
?>