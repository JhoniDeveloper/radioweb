<?php get_header(); ?>
<section class="custom_slider">
	<?php echo do_shortcode("[rev_slider home]"); ?>
</section>
<section class="curitiba_nerd">
	<div class="container">
		<h1 class="title">CURITIBA NERD</h1>
		<div class="row posts owl-carousel">
			<?php
				// $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
				$args = array(
					'post_type' 	=> 'post',
					'order'     	=> 'DESC',
					'category_name'  => 'curitiba-nerd',
					// 'posts_per_page'=> '3',
					// 'paged'         => $paged,
					'post_status'	=> 'publish'
				);
				$wc_query = new WP_Query( $args );

	            if ($wc_query -> have_posts()):
	                while ($wc_query -> have_posts()): $wc_query -> the_post();
						$thumb_id = get_post_thumbnail_id();
						$thumb_url = wp_get_attachment_url( $thumb_id );	
						$cat = get_the_category( get_the_ID() );	                	
			?>
			<a href="<?php the_permalink(); ?>">
				<div class="artigo">					
					<article>
						<figure>
							<img src="<?php echo $thumb_url; ?>">
						</figure>
						<div class="desc">							
							<div class="align_Desc"><p><?php echo esc_html($cat[0]->name); ?></p></div>									
							<h3><?php the_title(); ?></h3>								
						</div>				
					</article>
				</div>
			</a>
			<?php
				endwhile;				
				endif;
				wp_reset_query();
			?>
		</div>
	</div>
</section>

<section class="ultimas_noticias">
	<div class="container">
		<h1 class="title">ÚLTIMOS ARTIGOS</h1>
		<div class="row">
			<div class="col-lg-9 col-md-9 col-sm-12 col-12 no-padding">
				<div class="row posts">
					<div class="col-lg-8 no-padding">
						<?php
							// $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
							$args = array(
								'post_type' 	=> 'post',
								'category_name'  => 'games',
								'order'     	=> 'DESC',

								'posts_per_page'=> '1',
								'post_status'	=> 'publish'
							);
							$wc_query = new WP_Query( $args );

				            if ($wc_query -> have_posts()):
				                while ($wc_query -> have_posts()): $wc_query -> the_post();
									$thumb_id = get_post_thumbnail_id();
									$thumb_url = wp_get_attachment_url( $thumb_id );	
									$cat = get_the_category( get_the_ID() );	                	
						?>
						<a href="<?php the_permalink(); ?>">
							<div class="artigo">					
								<article>
									<figure>
										<img src="<?php echo $thumb_url; ?>">
									</figure>
									<div class="desc">
										<div class="align_desc"><p><?php echo esc_html($cat[0]->name); ?></p></div>
										<h3><?php the_title(); ?></h3>								
									</div>				
								</article>
							</div>
						</a>
						<?php
							endwhile;				
							endif;
							wp_reset_query();
						?>						
					</div>
					<div class="col-lg-4">
						<?php
							// $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
							$args = array(
								'post_type' 	=> 'post',
								'category_name'  => 'series',
								'order'     	=> 'DESC',
								'posts_per_page'=> '1',
								'post_status'	=> 'publish'
							);
							$wc_query = new WP_Query( $args );

				            if ($wc_query -> have_posts()):
				                while ($wc_query -> have_posts()): $wc_query -> the_post();
									$thumb_id = get_post_thumbnail_id();
									$thumb_url = wp_get_attachment_url( $thumb_id );	
									$cat = get_the_category( get_the_ID() );	                	
						?>
						<a href="<?php the_permalink(); ?>">
							<div class="artigo">					
								<article>
									<figure>
										<img src="<?php echo $thumb_url; ?>">
									</figure>
									<div class="desc">
										<div class="align_Desc"><p><?php echo esc_html($cat[0]->name); ?></p></div>
										<h3><?php the_title(); ?></h3>											
									</div>									
								</article>
								<div class="posts_conteudo"><?php $aux = get_the_content(); print(limitarTexto($aux, $limite = 400));?></div>
							</div>
						</a>
						<?php
							endwhile;				
							endif;
							wp_reset_query();
						?>							
					</div>
				</div>
				<div class="row posts">					
					<?php
						// $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
						$args = array(
							'post_type' 	=> 'post',
							'category_name'  => 'livros-hqs',
							'posts_per_page'=> '2',
							'order'     	=> 'DESC',
							'post_status'	=> 'publish'
						);
						$wc_query = new WP_Query( $args );

			            if ($wc_query -> have_posts()):
			                while ($wc_query -> have_posts()): $wc_query -> the_post();
								$thumb_id = get_post_thumbnail_id();
								$thumb_url = wp_get_attachment_url( $thumb_id );	
								$cat = get_the_category( get_the_ID() );	                	
					?>
					<div class="col-lg-4 no-padding-left">
						<a href="<?php the_permalink(); ?>">
							<div class="artigo">					
								<article>
									<figure>
										<img src="<?php echo $thumb_url; ?>">
									</figure>
									<div class="desc">
										<div class="align_Desc"><p><?php echo esc_html($cat[0]->name); ?></p></div>
										<h3><?php the_title(); ?></h3>
									</div>									
								</article>
							</div>
						</a>
					</div>
					<?php
						endwhile;				
						endif;
						wp_reset_query();
					?>		
					<?php
						// $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
						$args = array(
							'post_type' 	=> 'post',
							'category_name'  => 'filmes',
							'cat'			=> '-33',							
							'posts_per_page'=> '1',
							'order'     	=> 'DESC',
							'post_status'	=> 'publish'
						);
						$wc_query = new WP_Query( $args );

			            if ($wc_query -> have_posts()):
			                while ($wc_query -> have_posts()): $wc_query -> the_post();
								$thumb_id = get_post_thumbnail_id();
								$thumb_url = wp_get_attachment_url( $thumb_id );	
								$cat = get_the_category( get_the_ID() );	                	
					?>
					<div class="col-lg-4 no-padding-left">
						<a href="<?php the_permalink(); ?>">
							<div class="artigo">					
								<article>
									<figure>
										<img src="<?php echo $thumb_url; ?>">
									</figure>
									<div class="desc">
										<div class="align_Desc"><p><?php echo esc_html($cat[0]->name); ?></p></div>
										<h3><?php the_title(); ?></h3>
									</div>									
								</article>
							</div>
						</a>
					</div>
					<?php
						endwhile;				
						endif;
						wp_reset_query();
					?>											
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-12 col-12 no-padding-left no-padding-right randomicos posts">
				<?php
					// $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
					$args = array(
						'post_type' 	=> 'post',
						'order'     	=> 'DESC',
						'orderby'		=> 'rand',
						'cat'			=> '-33',
						'posts_per_page'=> '3',
						'post_status'	=> 'publish'
					);
					$wc_query = new WP_Query( $args );

		            if ($wc_query -> have_posts()):
		                while ($wc_query -> have_posts()): $wc_query -> the_post();
							$thumb_id = get_post_thumbnail_id();
							$thumb_url = wp_get_attachment_url( $thumb_id );	
							$cat = get_the_category( get_the_ID() );	                	
				?>
					<a href="<?php the_permalink(); ?>">
						<div class="artigo">					
							<article>
								<figure>
									<img src="<?php echo $thumb_url; ?>">
								</figure>
								<div class="desc">
									<div class="align_Desc"><p><?php echo esc_html($cat[0]->name); ?></p></div>
									<h3><?php the_title(); ?></h3>
								</div>									
							</article>
						</div>
					</a>
				<?php
					endwhile;				
					endif;
					wp_reset_query();
				?>				
			</div>
		</div>
	</div>
</section>
<section class="ultimas_criticas">
	<div class="container">
		<h1 class="title">ÚLTIMAS CRÍTICAS</h1>
		<div class="row posts owl-carousel">
			<?php
				// $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
				$args = array(
					'post_type' 	=> 'post',
					'order'     	=> 'DESC',
					'category_name'  => 'criticas',
					// 'posts_per_page'=> '3',
					// 'paged'         => $paged,
					'post_status'	=> 'publish'
				);
				$wc_query = new WP_Query( $args );

	            if ($wc_query -> have_posts()):
	                while ($wc_query -> have_posts()): $wc_query -> the_post();
						$thumb_id = get_post_thumbnail_id();
						$thumb_url = wp_get_attachment_url( $thumb_id );	
						$cat = get_the_category( get_the_ID() );	                	
			?>
			<a href="<?php the_permalink(); ?>">
				<div class="artigo">					
					<article>
						<figure>
							<img src="<?php echo $thumb_url; ?>">
						</figure>				
					</article>
					<h3 class="posts_titulo"><?php the_title(); ?></h3>	
				</div>
			</a>
			<?php
				endwhile;				
				endif;
				wp_reset_query();
			?>
		</div>
	</div>
</section>
<section class="canal_youtube">
	<div class="container">
		<h1 class="title no-margin-bottom">INSCREVA - SE EM NOSSO CANAL</h1>
		<div style="text-align: center; background-color: #333;">
			<a href="https://www.youtube.com/channel/UC0NDgg4BcqVrjLVxHxSjxCw" target="_blank" class="botao_youtube hvr-wobble-horizontal">Inscreva - se</a>
		</div>
		<div class="row posts owl-carousel">
			<?php
				// $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
				$args = array(
					'post_type' 	=> 'post',
					'order'     	=> 'DESC',
					'category_name'  => 'youtube',
					// 'posts_per_page'=> '3',
					// 'paged'         => $paged,
					'post_status'	=> 'publish'
				);
				$wc_query = new WP_Query( $args );

	            if ($wc_query -> have_posts()):
	                while ($wc_query -> have_posts()): $wc_query -> the_post();
						$thumb_id = get_post_thumbnail_id();
						$thumb_url = wp_get_attachment_url( $thumb_id );	
						$cat = get_the_category( get_the_ID() );	                	
			?>
			<a href="<?php the_permalink(); ?>">
				<div class="artigo">					
					<article>
						<figure>
							<img src="<?php echo $thumb_url; ?>">
						</figure>				
					</article>
					<h3 class="posts_titulo"><?php the_title(); ?></h3>	
				</div>
			</a>
			<?php
				endwhile;				
				endif;
				wp_reset_query();
			?>
		</div>
	</div>
</section>
<?php get_footer(); ?>