<?php
	get_header();
	bg_page();
?>	
	<section class="sobre_nos">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-md-6 col-sm-6">
					<?php echo do_shortcode('[rev_slider quem-somos]');?>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6">
					<h2>Quem Somos</h2>
					<div class="border_div"></div>

					<p>
						Com 17 anos de experiência no ramo, somos uma empresa especializada em <b>TONERS</b>, <b>CARTUCHOS</b>, <b>MANUTENÇÃO DE IMPRESSORAS</b> e <b>BULK INK</b>. Também somos plantadores de <b>árvores</b> e preservamos o <b>meio ambiente</b> como uma das principais missões de nossa empresa, a <b>PRINT EXPRESS</b> tem cultivado e priorizado o que por inconsequência do homem está esquecido, com um simples gesto você e sua empresa também podem fazer à diferença, pensando em você nós temos a certificação e descartamos com segurança  todo resíduo e lixo  de nossos clientes.
					</p>
					<p>
						Nosso sucesso é fruto de muito trabalho. Nossa equipe especializada é focada em nosso principal objetivo: atender ao público com qualidade e agilidade, oferecendo ao cliente o que há de melhor no mercado.
					</p>
					<p>
						Com sede própria no bairro Água Verde na rua Prof. Assis Gonçalves, nº 111, estamos em uma ótima localização de Curitiba para melhor atende-lo. Contamos com uma frota de motoboys especializados e treinados que entregam com agilidade e trocam os suprimentos. 
					</p>
					<p>
						Somos sem dúvida a melhor empresa em confiabilidade e qualidade em se tratando de <b>MANUTENÇÃO DE IMPRESSORA</b>, <b>OUTSOURCING DE IMPRESSÃO</b>, <b>TONER</b> <b>CARTUCHO</b> E <b>BULK INK</b>.
					</p>
					<p>	
						Com mais de 16 anos no mercado, consolidamos nossa marca com valor, virtude e excelência nas atividades desenvolvidas pela empresa. Ostentamos orgulhosmente como resultado de nosso magnífico trabalho, um portfólio com mais de 12.000 clientes satisfeitos. 
					</p>
					<p>
						Não fique de fora! Venha plantar sua <b>árvore</b> no mundo, compre seu suprimento mensal e nós faremos juntos a diferença.
					</p>
				</div>					
			</div>
		</div>	
	</section>
	<section class="sobre_desc">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-md-6 col-sm-6">
				<h2>Ação Social</h2>
				<div class="border_div"></div>				
					<p>
						Nós da <b>Print Express</b> acreditamos em caridade e solidariedade, caridade é tão vertical e solidariedade é horizontal, respeita a outra pessoa e aprende com o outro. A maioria de nós tem muito o que aprender com as outras pessoas. Acreditamos que fazer o bem a nossa humanidade é um dever, porém é um dever prazeroso de ser cumprido, pois plantamos a semente do bem em corações que muitas vezes perderão a esperança.
					</p>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6">
					<iframe width="100%" height="315" src="https://www.youtube.com/embed/vqld2ieS_nI?rel=0" frameborder="0" allowfullscreen></iframe>
				</div>
			</div>
		</div>
	</section>
	<section class="caracteristicas_sobre">
		<div class="container">
			<div class="row">
				<div class="col-lg-4 col-md-4 col-sm-4">
					<h2 class="title_div">NOSSA MISSÃO</h2>
					<div class="border_div"></div>
					<P>						
						Oferecer soluções de <b>impressões</b> rápidas e de qualidade atendendo a necessidade e a satisfação do cliente, objetivando novos parceiros para o crescimento contínuo.

					</P>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4">
					<h2 class="title_div">NOSSA VISÃO</h2>
					<div class="border_div"></div>		
					<p>						
						Ser referência em <b>outsourcing de impressão</b>, suprimentos e <b>manutenção de equipamentos</b> (<b>impressoras</b>, computadores) no mercado, com aprimoramento e inovação.
					</p>		
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4">
					<h2 class="title_div">NOSSOS VALORES</h2>
					<div class="border_div"></div>
					<p>						
						Os valores da <b>Print Express Brasil</b> revelados através do profissionalismo, respeito, confiança, credibilidade, perseverança e competência nos serviços prestados.
					</p>
				</div>
				<p style="padding: 15px;"><span style="color: red;">OBJETIVO:</span> Chegar ao topo do sucesso.
</p>
			</div>
		</div>
	</section>
	<section class="clientes_sobre">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-md-6 col-sm-6">
					<h2 class="title_div">ALGUNS DE NOSSOS CLIENTES</h2>
					<div class="border_div"></div>

					<img src="<?php echo get_bloginfo("template_url"); ?>/_assets/img/sobre/tim.png">
					<img src="<?php echo get_bloginfo("template_url"); ?>/_assets/img/sobre/uci.png">				
					<img src="<?php echo get_bloginfo("template_url"); ?>/_assets/img/sobre/madero.png">
					<img src="<?php echo get_bloginfo("template_url"); ?>/_assets/img/sobre/max-flex.png">
					<img src="<?php echo get_bloginfo("template_url"); ?>/_assets/img/sobre/mini-preco.png">
					<img src="<?php echo get_bloginfo("template_url"); ?>/_assets/img/sobre/aqua.png">
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6">
					<h2 class="title_div">O QUE OS CLIENTES DIZEM</h2>
					<div class="border_div"></div>

					<div class="texto">
						<p>
							PRINT EXPRESS BRASIL junto com você na preservação do
							NOSSO PLANETA. Em parceria com a Secretaria do Meior
							Ambiente realizamos coleta de cartuchos e toners em
							nossos clientes evitando assim o descarte indevido.
							Cartuchos, toners e tinta são recolhidos e devidamente.						
						</p>
					</div>				
				</div>				
			</div>
		</div>
	</section>
	<section class="gerenciar_custos">
		<div class="container">
			<div class="row">
				<h3>Deseja GERENCIAR e CONTROLAR os CUSTOS com impressão na sua empresa?</h3>
				<p>Contrate uma consultoria de Outsourcing de Impressão!</p>
				<a href="<?php echo get_bloginfo('url') ;?>contato/" class="hvr-wobble-horizontal">Quero contratar uma consultoria de outsourcing!</a>
			</div>
		</div>
	</section>	
<?php get_footer(); ?>
