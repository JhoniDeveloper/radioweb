<footer>
	<div class="container">
		<div class="row">
			<div class="col-lg-4 col-md-4 col-sm-12 col-12 um">
				<div>
<iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fwearenerdnation%2F&tabs=timeline&width=350&height=480&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=1171923162952354" width="100%" height="480" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
				</div>
				
			</div>
			<div class="col-lg-4 col-md-4 col-sm-12 col-12 dois">
				<h1 class="title_footer">FALE CONOSCO</h1>
				<div class="form_footer">
					<?php echo do_shortcode('[contact-form-7 id="45" title="Contato"]'); ?>
				</div>
				<div class="footer_ou">CURTA OU COMPARTILHE NOSSA PÁGINA</div>
				<center>
					<div id="fb-root"></div>
					<script>(function(d, s, id) {
					var js, fjs = d.getElementsByTagName(s)[0];
					if (d.getElementById(id)) return;
					js = d.createElement(s); js.id = id;
					js.src = 'https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.11&appId=1171923162952354';
					fjs.parentNode.insertBefore(js, fjs);
					}(document, 'script', 'facebook-jssdk'));</script>
					<div class="fb-like" data-href="https://www.facebook.com/wearenerdnation/" data-layout="button_count" data-action="like" data-size="large" data-show-faces="true" data-share="true"></div>
				</center>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-12 col-12 tres">
				<h1 class="title_footer">INSTAGRAM</h1>
				<div class="insta_footer">
					<?php echo do_shortcode('[instagram-feed]'); ?>
				</div>		
				<h1 class="title_footer">SIGA - NOS</h1>	
				<div class="footer_sociais">
					<a href="https://www.facebook.com/wearenerdnation/" target"_blank">
						<i class="fa fa-facebook-square" aria-hidden="true"></i>
					</a>
					<a href="" target"_blank">
						<i class="fa fa-instagram" aria-hidden="true"></i>
					</a>					
				</div>
			</div>
		</div>
	</div>
	<div class="copy">
		<div class="container no-padding">
			<div class="row">
				<div class="col-lg-6 col-md-6 col-sm-12 col-12" style="text-align: left; color: #fff;">
					<p>
						©2018 NerdNation, Todos os direitos reservados.
					</p>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-12 col-12" style="text-align: right; color: #fff;">
					<a href="http://www.jhonatan.eti.br" target="_blank" style="color: #fff;">
						Desenvolvimento: Jhonatan Paulo - Especialista em Tecnologias de Informação.
					</a>
				</div>
			</div>		
		</div>		
	</div>
</footer>

</body>
</html>